import azure.functions as func
import azure.durable_functions as df
from azure.storage.blob import BlobServiceClient

myApp = df.DFApp(http_auth_level=func.AuthLevel.ANONYMOUS)


@myApp.route(route="orchestrators/{functionName}")
@myApp.durable_client_input(client_name="client")
async def http_start(req: func.HttpRequest, client):
    function_name = req.route_params.get('functionName')
    instance_id = await client.start_new(function_name,None)
    response = client.create_check_status_response(req, instance_id)
    return response

@myApp.orchestration_trigger(context_name="context")
def master_orchestrator(context):
    #inputData ={"1":"With tuppence for paper and strings You can have your own set of wings With your feet on the ground","2":"With tuppence for paper and strings You can have your own set of wings With your feet on the ground"}   
    inputData = yield context.call_activity("GetInputDataFn","")
    print(inputData)
     # Fan-out: Map each input value using the mapper function
    mapLines = []
    for key, value in inputData.items():
        mapLines.append(context.call_activity("mapper", {key: value}))
        
    mapOutputs = yield context.task_all(mapLines)
    # Fan-in: Shuffle the mapped outputs using the shuffler function
    shuffledData = yield context.call_activity("shuffler", mapOutputs)
    # Fan-out: Reduce the shuffled data using the reducer function
    reduceWords = []
    for key, values in shuffledData.items():
        reduceWords.append(context.call_activity("reducer", {key: values}))
    # Fan-in: Aggregate the reduced results
    finalResult = yield context.task_all(reduceWords)
    return finalResult

@myApp.activity_trigger(input_name="inputData")
def mapper(inputData):
    output = []
    for key, value in inputData.items():
        words = value.split()
        for word in words:
            output.append((word, 1))
    return output
    
@myApp.activity_trigger(input_name="mapOutputs")
def shuffler(mapOutputs):
    shuffle_dict = {}
    for output_list in mapOutputs:
        for key, value in output_list:
            if key in shuffle_dict:
                shuffle_dict[key].append(value)
            else:
                shuffle_dict[key] = [value]
    
    return shuffle_dict

@myApp.activity_trigger(input_name="shuffledData")
def reducer(shuffledData):
    return {key: sum(values) for key, values in shuffledData.items()}

@myApp.activity_trigger(input_name="unusedData")
def GetInputDataFn(unusedData):
    connection_string = "DefaultEndpointsProtocol=https;AccountName=testorches;AccountKey=vRo8gwsIlshN3gZ3XW/qjRlsQMGf4xZB8Gs7sBqbTLA80L+DJfHyhnviRQTiSdPYdfkuRh8Vxj/h+AStNxPFgA==;EndpointSuffix=core.windows.net"
    container_name = "orches-lab2-p5-blob"
    blob_service_client = BlobServiceClient.from_connection_string(connection_string)
    container_client = blob_service_client.get_container_client(container_name)
    blobs = container_client.list_blobs()
    input_data_dict = {}
    offset = 0
    # Iterate through each blob to get the content (assuming text file) and build key-value pairs
    for blob in blobs:
        blob_client = container_client.get_blob_client(blob.name)
        blob_content = blob_client.download_blob().readall().decode('utf-8')
        for line in blob_content.splitlines():
            
            input_data_dict[str(offset)]=line 
            offset +=1

    return input_data_dict



