from flask import Flask, jsonify, request
import math

app = Flask(__name__)

def function_to_integrate(x):
    return abs(math.sin(x))

def numerical_integration(lower, upper, N):
    dx = (upper - lower) / N
    integral = 0.0

    for i in range(N):
        x_left = lower + i * dx
        x_right = x_left + dx
        area = dx * (function_to_integrate(x_left) + function_to_integrate(x_right)) / 2
        integral += area

    return integral


@app.route('/numericalintegralservice')
def numerical_integration_service():
    lower = float(request.args.get('lower', default=0.0))
    upper = float(request.args.get('upper', default=math.pi))  # Default to pi if not provided

    N_values = [10, 100, 1000, 10000, 100000, 1000000]
    results = {}

    for N in N_values:
        result = numerical_integration(lower, upper, N)
        results[N] = result

    return jsonify(results)

if __name__ == '__main__':
    app.run(debug=True)
