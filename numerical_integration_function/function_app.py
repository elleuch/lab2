import azure.functions as func
import logging
import math

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)
def function_to_integrate(x):
    return abs(math.sin(x))

def numerical_integration(lower, upper, N):
    dx = (upper - lower) / N
    integral = 0.0

    for i in range(N):
        x_left = lower + i * dx
        x_right = x_left + dx
        area = dx * (function_to_integrate(x_left) + function_to_integrate(x_right)) / 2
        integral += area

    return integral

@app.route(route="numericalintegralservice")
def numerical_integration_service(req: func.HttpRequest) -> func.HttpResponse:
    try:
        lower = float(req.params.get('lower', 0.0))
        upper = float(req.params.get('upper', math.pi))
    except ValueError as e:
        return func.HttpResponse("Invalid input. Please provide valid numeric values for 'lower' and 'upper'.", status_code=400)

    N_values = [10, 100, 1000, 10000, 100000, 1000000]
    results = {}

    for N in N_values:
        result = numerical_integration(lower, upper, N)
        results[N] = result

    return func.HttpResponse(f"Numerical integration results: {results}", status_code=200)
