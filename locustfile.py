from locust import HttpUser, task, between

class NumericalIntegrationUser(HttpUser):
    wait_time = between(1, 2)  # time between requests, in seconds
    #host= "http://104.45.207.3:80"
    #host= "https://lab2python.azurewebsites.net"
    #host= "http://127.0.0.1:5000"
    host ="https://numerical-integration-function.azurewebsites.net/api"
    @task
    def numerical_integration_task(self):
        lower = 0.0
        upper = 3.14159

        response = self.client.get(f'/numericalintegralservice?lower={lower}&upper={upper}')
        if response.status_code != 200:
            print(f"Error: {response.status_code}, Content: {response.content}")
